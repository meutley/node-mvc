# Getting Started #

Technologies used:

* nodejs
* expressjs
* pug (formerly jade)
* bower
* bootstrap
* jquery
* mocha
* chai
* sinon
* gulp

Global dependencies:

* install mocha: npm install mocha -g
* install bower: npm install bower -g

To build the app:

* node dependencies: npm install
* client dependencies: bower install

To run tests, run: npm test

To run the app:

* node index.js
* Point your browser to http://localhost:3000/