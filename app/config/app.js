module.exports = {
    global: {
        appRoot: 'app'
    },
    
    server: {
        port: 3000
    },
    
    web: {
        defaultControllerName: 'home',
        defaultActionName: 'index',
        controllerModuleName: 'controller',
        viewEngine: {
            name: 'pug'
        }
    },
    
    staticContent: {
        paths: [
            // Third party libraries
            { mountPoint: '/lib/bootstrap', path: 'libs/bootstrap/dist' },
            { mountPoint: '/lib/jquery', path: 'libs/jquery/dist' },
            
            // App content
            { mountPoint: '/app/css', path: 'site/style' }
        ]
    }
}