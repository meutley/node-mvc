var routeConfig = require('../web/routeConfig');

var App = App || {};
App.Home = App.Home || {
    viewRoot: 'home/'
};

App.Home.Views = {
    index: App.Home.viewRoot + 'index'
};

routeConfig.controllers.home = {
    get_index: function (request, response) {
        response.render(App.Home.Views.index);
    }
}