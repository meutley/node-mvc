var fs = require('fs');
var path = require('path');

/*
    Dynamically loads and 'require's all controllers in the application.
*/
module.exports.loadControllers = function (rootPath, controllerModuleName) {
    // Locate directories in the root folder
    fs.readdirSync(rootPath).filter(function(file) {
        var isDirectory = fs.statSync(path.join(rootPath, file)).isDirectory();
        if (isDirectory) {
            // Locate and load ('require') the 'controller' module, if any
            var loadPath = path.join(file, controllerModuleName);
            var fullPath = path.join(rootPath, file, controllerModuleName + '.js');
            
            fs.stat(fullPath, function (err, stat) {
                if (!err) {
                    require(path.join('..', '..', rootPath, loadPath));
                }
            });
        }
    });
}