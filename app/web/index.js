// Middleware
var applicationErrorHandler = require('../middleware/applicationErrorHandler');

// Configuration
var routeConfig = require('./routeConfig');

// Services
var viewEngineService = require('./viewEngineService');
var controllerService = require('./controllerService');
var staticContentService = require('./staticContentService');

module.exports = {
    init: function (app, config) {
        // Controllers and view engine
        controllerService.loadControllers(config.global.appRoot, config.web.controllerModuleName);
        viewEngineService.init(app, config.web.viewEngine.name, config.global.appRoot);
        
        // Static content
        staticContentService.init(app, config.staticContent.paths);
        
        // Routing
        routeConfig.init(app, config);
        
        // Error handler middleware
        app.use(applicationErrorHandler);
    }
}