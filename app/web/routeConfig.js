var routeConfigUtil = require('./routeConfigUtil');
var httpVerbs = require('../common/httpVerbs');

module.exports = {
    // controllers contains each of the app's registered controllers
    controllers: {},
    
    // register route handlers here
    init: function (app, config) {
        var self = this;
        
        routeConfigUtil.defaultControllerName = config.web.defaultControllerName;
        routeConfigUtil.defaultActionName = config.web.defaultActionName;
        
        app.use('/:controller?/:action?/:id?', function (request, response, next) {
            var verb = request.method.toLowerCase();
            var httpVerb = httpVerbs[verb];
            if (httpVerb) {
                routeConfigUtil.defaultHandler(httpVerb, self.controllers, request, response);
            } else {
                response.sendStatus(500);
            }
            
            next();
        });
    }
}