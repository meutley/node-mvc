var httpStatusCodes = require('../common/httpStatusCodes');

/*
    Builds a full action name using the verb and base action name.
    
    e.g. get_Index, post_Save
*/
var getActionMethodName = function (verb, baseActionName) {
    var verbPrefix = verb.toLowerCase() + '_';
    var actionWithVerb = verbPrefix + baseActionName;
    
    return actionWithVerb;
}

module.exports = {
    defaultControllerName: '',
    defaultActionName: '',
    
    /*
        Generic route handler
    */
    defaultHandler: function (verb, controllers, request, response) {
        var controllerName = request.params.controller || this.defaultControllerName;
        var useAction = request.params.action || this.defaultActionName;

        var controller = controllers[controllerName];
        if (controller) {
            // Get the action method
            var actionName = getActionMethodName(verb, useAction);
            var action = controller[actionName];
            
            // Execute the action, if valid, on the given controller
            if (action && typeof action === 'function') {
                action(request, response);
            } else {
                response.sendStatus(httpStatusCodes.notFound);    // Action method not found
            }
        } else {
            response.sendStatus(httpStatusCodes.notFound);    // Controller not found
        }
    }
}