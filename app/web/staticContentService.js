var express = require('express');
var path = require('path');

var publicPath = path.join(__dirname, '..', '..', 'public');

module.exports = {
    init: function (app, staticContentPaths) {
        if (staticContentPaths && staticContentPaths.length > 0) {
            for (var x = 0; x < staticContentPaths.length; x++) {
                // At the given mount point, service the static content from the corresponding path
                var mountPoint = staticContentPaths[x].mountPoint;
                var mountPath = staticContentPaths[x].path;
                var fullPath = path.join(publicPath, mountPath);
                
                app.use(mountPoint, express.static(fullPath));
            }
        }
    }
}