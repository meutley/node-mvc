var path = require('path');

var publicPath = path.join(__dirname, '..', '..', 'public');

module.exports = {
    init: function (app, viewEngineName, viewRoot) {
        app.set('view engine', viewEngineName);
        app.set('views', viewRoot);
    }
}