var express = require('express');
var app = express();

var env = process.env.NODE_ENV || 'debug';

// Configuration
var cfg = require('./app/config');
var config = cfg.getConfig(env);

// Web server
var web = require('./app/web');
web.init(app, config);

var server = app.listen(config.server.port, function () {
    var host = server.address().address;
    host = (host === '::') ? 'localhost' : host;
    var port = server.address().port;
    
    console.log('Server is listening at http://%s:%s', host, port);
});