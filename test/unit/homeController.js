var chai = require('chai');
var sinon = require('sinon');

var expect = chai.expect;

var express = require('express');
var app = express();

describe('homeController', function () {
    beforeEach(function () {
        this.routeConfig = require('../../app/web/routeConfig');
        require('../../app/home/controller');
    });
    
    it('should register in routeConfig', function () {
        expect(this.routeConfig.controllers.home.get_index).to.not.be.null;
    });
    
    it('get index should call response.render', function () {
        var renderPath = 'home/index';
        var response = {
            render: function (path) {
                
            }
        };
        
        var renderSpy = sinon.spy(response, "render");
        this.routeConfig.controllers.home.get_index(null, response);
        
        expect(this.routeConfig.controllers.home.get_index).to.not.be.null;
        sinon.assert.calledWith(renderSpy, renderPath);
    });
});