var chai = require('chai');
var sinon = require('sinon');

var expect = chai.expect;

describe('routeConfigUtil', function () {
    beforeEach(function () {
        this.homeController = {
            index: function (req, res) {
                
            }
        };
        
        this.controllers = {
            home: {
                get_index: function (req, res) {
                    
                }
            }
        };
        
        this.response = {
            sendStatus: function (code) {
                
            }
        }
        
        this.routeConfigUtil = require('../../app/web/routeConfigUtil');
        
        this.httpStatusCodes = require('../../app/common/httpStatusCodes');
    });
    
    it('GET / should call default home/index route', function () {
        this.homeIndexSpy = sinon.spy(this.controllers.home, "get_index");
        
        var request = {
            params: {
                controller: 'home',
                action: 'index'
            }
        };
        
        this.routeConfigUtil.defaultHandler('GET', this.controllers, request, this.response);
        
        sinon.assert.calledOnce(this.homeIndexSpy);
    });
    
    it('GET home/index should call index action on home controller', function () {
        this.homeIndexSpy = sinon.spy(this.controllers.home, "get_index");
        
        var request = {
            params: {
                controller: 'home',
                action: 'index'
            }
        };
        
        this.routeConfigUtil.defaultHandler('GET', this.controllers, request, this.response);
        
        sinon.assert.calledOnce(this.homeIndexSpy);
    });
    
    it('GET home/index/1 should call index action on home controller', function () {
        this.homeIndexSpy = sinon.spy(this.controllers.home, "get_index");
        
        var request = {
            params: {
                controller: 'home',
                action: 'index',
                id: 1
            }
        };
        
        this.routeConfigUtil.defaultHandler('GET', this.controllers, request, this.response);
        
        sinon.assert.calledOnce(this.homeIndexSpy);
    });
    
    it('GET invalid action should send HTTP status code 404', function () {
        this.sendStatusSpy = sinon.spy(this.response, "sendStatus");
        
        var request = {
            params: {
                controller: 'home',
                action: 'asdf'
            }
        };
        
        this.routeConfigUtil.defaultHandler('GET', this.controllers, request, this.response);
        
        sinon.assert.calledWith(this.sendStatusSpy, this.httpStatusCodes.notFound);
    });
    
    it('GET invalid controller should send HTTP status code 404', function () {
        this.sendStatusSpy = sinon.spy(this.response, "sendStatus");
        
        var request = {
            params: {
                controller: 'fake',
                action: 'index'
            }
        };
        
        this.routeConfigUtil.defaultHandler('GET', this.controllers, request, this.response);
        
        sinon.assert.calledWith(this.sendStatusSpy, this.httpStatusCodes.notFound);
    });
});