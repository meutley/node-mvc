var chai = require('chai');
var sinon = require('sinon');

describe('viewEngineService', function () {
    beforeEach(function () {
        this.viewEngineService = require('../../app/web/viewEngineService');
        
        this.viewEngine = 'html';
        this.viewRoot = '/views';
        
        this.app = {
            set: function (key, value) {
                
            }
        };
        
        this.spy = sinon.spy(this.app, "set");
    });
    
    it('init should set the view engine and root views path', function () {
        this.viewEngineService.init(this.app, this.viewEngine, this.viewRoot);
        
        sinon.assert.calledWith(this.spy, 'view engine', this.viewEngine);
        sinon.assert.calledWith(this.spy, 'views', this.viewRoot);
    });
});